﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DimeRental
{
    public partial class AgreementForm : Form
    {
        public string status;

        public AgreementForm()
        {
            InitializeComponent();
        }

        private void AgreementForm_Load(object sender, EventArgs e)
        {
            dtpExpiry.Format = DateTimePickerFormat.Custom;
            dtpExpiry.CustomFormat = "MM/yyyy";
            dtpExpiry.ShowUpDown = true;
            Agreement agreement = new Agreement();
            string[,] vehicles = new string[100, 5];
            vehicles = agreement.getVehicles();

            for (int x = 0; x < vehicles.GetLength(0); x++)
            {
                if (vehicles[x, 0] != null)
                    cmbVehicle.Items.Add(vehicles[x, 0] + " - " + vehicles[x, 1] + " " + vehicles[x, 2]);
            }

            cmbVehicle.SelectedIndex = 0;

            cmbProv.SelectedIndex = 8;
            cmbDriverProv.SelectedIndex = 8;
            if (status == "Close")
            {
                //Allows for the user to enter the closing data.
                txtKmIn.Enabled = true;
                txtFuelIn.Enabled = true;

                //TODO:
                //Gather pricing from days & km rented.
            }


        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            string insuranceType = getInsuranceType();
           
            if (status != "Close")
                if (checkInputs())
                {
                    long.TryParse(txtPhone.Text, out long phone);
                    long.TryParse(txtDriversLicense.Text, out long driversLicNum);
                    long.TryParse(txtKmOut.Text, out long kmout);
                    Agreement newAgreement = new Agreement(txtRentFirst.Text, txtRentLast.Text, txtCustFirst.Text, txtCustLast.Text,
                        phone, txtEmail.Text, driversLicNum, (dtpExpiry.Text), dtpBirthday.Value.ToString(), txtAddress.Text, txtCity.Text, cmbProv.Items[cmbProv.SelectedIndex].ToString(),
                        cmbDriverProv.Items[cmbDriverProv.SelectedIndex].ToString(), txtPostal.Text, cmbVehicle.Items[cmbVehicle.SelectedIndex].ToString().Substring(0,7), insuranceType, txtPolicyNo.Text, dtpOut.Value, kmout);
                    newAgreement.openRental();
                }
        }

        public string getInsuranceType()
        {
            string type = "";
            RadioButton[] radios = new RadioButton[] { radNone, radOWC, radNoDed };
            foreach (RadioButton x in radios)
            {
                if (x.Checked)
                    type = x.Text;
            }
            return type;
        }

        //Checks all text fields and makes sure they are not left empty.
        public bool checkInputs()
        {
            string missing = "";
            bool flag = true;
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    TextBox textBox = c as TextBox;
                    if (textBox.Text == string.Empty)
                    {
                        missing = textBox.Name.Remove(0, 3) + "\n" + missing;
                        flag = false;
                    }
                }
            }
            if (!flag)
                MessageBox.Show("Please fill in: \n" + missing, "Missing Information");
            return flag;
        }
    }
}
