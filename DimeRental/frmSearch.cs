﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DimeRental
{
    public partial class frmSearch : Form
    {

        SQLiteConnection sql_connection;
        SQLiteCommand sql_command;
        SQLiteDataReader sqlite_datareader;
        DataSet data_set;
        SQLiteDataAdapter sql_data_adapter;
        SQLiteCommandBuilder sql_command_builder;

        public frmSearch()
        {
            InitializeComponent();
        }
        string selectValue = "";
        string cmd = "";

        string[] searchables = new string[6];

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string wantedSearch = cmbField.Items[cmbField.SelectedIndex].ToString();
            if (txtValue.Text != string.Empty)
            {
                switch (cmbField.SelectedIndex)
                {
                    //First Name
                    case 0:
                        selectValue = "fname";
                        cmd = "select * from customer WHERE fname=:value";
                        break;

                    //Last Name
                    case 1:
                        selectValue = "lname";
                        cmd = "select * from customer WHERE lname=:value";
                        break;

                    //Phone Number
                    case 2:
                        selectValue = "phone";
                        cmd = "select * from customer WHERE phone=:value";
                        break;

                    //Open Date
                    case 3:
                        selectValue = "oDate";
                        cmd = "SELECT * FROM customer c INNER JOIN rentals r ON r.customer_id = c.customer_id WHERE r.oDate = :value";
                        break;

                    //Close Date
                    case 4:
                        selectValue = "cDate";
                        cmd = "SELECT * FROM customer c INNER JOIN rentals r ON r.customer_id = c.customer_id WHERE r.cDate = :value";
                        break;

                    case 5:
                        selectValue = "vehicle";
                        cmd = "SELECT * FROM customer c INNER JOIN rentals r ON r.customer_id = c.customer_id WHERE r.plate = :value";
                        break;
                }
                LoadData();
            }
            else
                MessageBox.Show("Please enter a value to search by", "Field Error");
        }

        public void SetConnection()
        {
            sql_connection = new SQLiteConnection(@"Data Source=dime.db");
            sql_connection.Open();
        }

        public void LoadData()
        {

            SetConnection();
            data_set = new DataSet();

            sql_command = sql_connection.CreateCommand();

            sql_data_adapter = new SQLiteDataAdapter(cmd, sql_connection);
            sql_data_adapter.SelectCommand.Parameters.AddWithValue(":value", txtValue.Text);
            sql_command_builder = new SQLiteCommandBuilder(sql_data_adapter);

            sql_data_adapter.Fill(data_set);

            dgvResult.DataSource = data_set.Tables[0].DefaultView;
        }

        private void frmSearch_Load(object sender, EventArgs e)
        {
            cmbField.SelectedIndex = 0;
        }
    }
}
