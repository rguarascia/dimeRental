﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace DimeRental
{
    public partial class frmVehicles : Form
    {
        public frmVehicles()
        {
            InitializeComponent();
        }

        SQLiteConnection sql_connection;
        SQLiteCommand sql_command;
        SQLiteDataReader sqlite_datareader;
        DataSet data_set;
        SQLiteDataAdapter sql_data_adapter;
        SQLiteCommandBuilder sql_command_builder;

        string mode = "";


        private void frmVehicles_Load(object sender, EventArgs e)
        {
            SetConnection();
            Loads();
        }

        //Sets up DB connections
        public void SetConnection()
        {
            sql_connection = new SQLiteConnection(@"Data Source=dime.db");
            sql_connection.Open();
        }

        //Gathers DB from the DB and sets up the DataGridView
        public void Loads()
        {
            SetConnection();
            data_set = new DataSet();

            sql_command = sql_connection.CreateCommand();
            string command_string = "select * from vehicles";

            sql_data_adapter = new SQLiteDataAdapter(command_string, sql_connection);
            sql_command_builder = new SQLiteCommandBuilder(sql_data_adapter);

            sql_data_adapter.Fill(data_set);

            dgvVehicleList.DataSource = data_set.Tables[0].DefaultView;

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            //Gets currently selected row and gathers the current data from the DB and sets txtboxs
            int row = dgvVehicleList.SelectedCells[0].RowIndex;
            Agreement agreement = new Agreement();
            string[,] vehicles = agreement.getVehicles();
            txtPlate.Text = vehicles[row, 0];
            txtMake.Text = vehicles[row,1];
            txtModel.Text = vehicles[row, 2];
            txtKM.Text = vehicles[row, 5];
            cmbClass.SelectedItem = vehicles[row, 4];

            //Allows user to edit
            txtPlate.Enabled = true;
            txtKM.Enabled = true;
            txtMake.Enabled = true;
            txtModel.Enabled = true;
            cmbClass.Enabled = true;
            btnSubmit.Enabled = true;

            mode = "edit";
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            //Makes sure the fields arent empty
            if (txtPlate.Text != null && txtMake.Text != null && txtModel.Text != null
                    && txtKM.Text != null && cmbClass.SelectedIndex != -1)
            {
                //Updates the currently selected vehicle
                try
                {
                    if (mode == "edit")
                    {
                        string dataSource = "dime.db";
                        using (SQLiteConnection connection = new SQLiteConnection())
                        {
                            connection.ConnectionString = "Data Source=" + dataSource;
                            connection.Open();
                            using (SQLiteCommand command = new SQLiteCommand(connection))
                            {
                                command.CommandText =
                                    "update vehicles set KM = :km where plate=:plate";
                                command.Parameters.Add("km", DbType.String).Value = txtKM.Text;
                                command.Parameters.Add("plate", DbType.String).Value = txtPlate.Text;
                                command.ExecuteNonQuery();
                            }
                        }
                    } else if (mode == "add")
                    {
                        string dataSource = "dime.db";
                        using (SQLiteConnection connection = new SQLiteConnection())
                        {
                            connection.ConnectionString = "Data Source=" + dataSource;
                            connection.Open();
                            using (SQLiteCommand command = new SQLiteCommand(connection))
                            {
                                command.CommandText =
                                    "INSERT into vehicles  (plate, make, model, rented, class, KM) VALUES (:plate, :make, :model, :rented, :class, :KM)";
                                command.Parameters.Add("plate", DbType.String).Value = txtPlate.Text;
                                command.Parameters.Add("make", DbType.String).Value = txtMake.Text;
                                command.Parameters.Add("model", DbType.String).Value = txtModel.Text;
                                command.Parameters.Add("rented", DbType.String).Value = 0;
                                command.Parameters.Add("class", DbType.String).Value = cmbClass.Items[cmbClass.SelectedIndex].ToString();
                                command.Parameters.Add("KM", DbType.String).Value = txtKM.Text;
                                command.ExecuteNonQuery();
                            }
                        }
                    }
                    //Displays the errors, if there are any.
                } catch (Exception ex)
                {
                    MessageBox.Show("Please include this message when emailing about a problem. This is the diagnostic information \n\nPress CTRL+C then paste into email body \n\n" + ex.ToString(), "We ran into a problem. Please contact rguarascia@gmail.com");
                }
                //refresh the grid view.
                finally
                {
                    Loads();
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Agreement agreement = new Agreement();
            string[,] vehicles = agreement.getVehicles();
            int row = dgvVehicleList.SelectedCells[0].RowIndex;

            DialogResult dialogResult = MessageBox.Show("Are you sure you would like to delete?\n\nVehicle: " + vehicles[row, 0]
                + " - " + vehicles[row, 1] + " " +  vehicles[row,2], "Some Title", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {

                    string dataSource = "dime.db";
                    using (SQLiteConnection connection = new SQLiteConnection())
                    {
                        connection.ConnectionString = "Data Source=" + dataSource;
                        connection.Open();
                        using (SQLiteCommand command = new SQLiteCommand(connection))
                        {
                            command.CommandText =
                                "DELETE FROM vehicles where plate=:plate";
                            command.Parameters.Add("plate", DbType.String).Value = vehicles[row,0];
                            command.ExecuteNonQuery();
                        }
                    }
                    //Displays the errors, if there are any.
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please include this message when emailing about a problem. This is the diagnostic information \n\nPress CTRL+C then paste into email body \n\n" + ex.ToString(), "We ran into a problem. Please contact rguarascia@gmail.com");
                }
                //refresh the grid view.
                finally
                {
                    Loads();
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //Allows user to edit
            txtPlate.Text = "";
            txtMake.Text = "";
            txtModel.Text = "";
            txtKM.Text = "";
            //Allows user to edit
            txtPlate.Enabled = true;
            txtKM.Enabled = true;
            txtMake.Enabled = true;
            txtModel.Enabled = true;
            cmbClass.Enabled = true;
            btnSubmit.Enabled = true;

            mode = "add";
        }
    }
}
