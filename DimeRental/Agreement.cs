﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data.Common;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace DimeRental
{
    class Agreement
    {
        string renterFname;
        string renterLname;
        string driverFname;
        string driverLname;
        long phonenumber;
        string email;
        long driversLicense;
        string driversLExpire;
        string driversLicenseBday;
        string driversProv;
        string address_string;
        string address_city;
        string address_prov;
        string address_country;
        string address_postalCode;
        string vehicle;
        string insuranceType;
        string insurancePolicy;
        string insurnacePrice;
        DateTime vehicleOut;
        long kmOut;
        DateTime vehicleIn;
        int kmIn;
        DateTime closeDate = new DateTime();
        int CPD; //Cost Per Day
        string vehicleType;

        //Forigen keys for the other tables. Very important.
        public int employeeID = 1;
        public long rentalID;
        public long insuranceID;
        public long customerID = 0;

        //Empty constructor for when we want to access classes here
        public Agreement() { }

        //Constructor for when the rental agreement is actually created
        public Agreement(string rentalFirst, string rentalLast, string cusFirst, string cusLast,
                long phoneN, string cusEmail, long driversLic, string driversExp, string bday, string address,
                string city, string prov, string licProv, string postal, string cusVehicle, string insurance, string policy, DateTime vOut, long kmO)
        {
            renterFname = rentalFirst;
            renterLname = rentalLast;
            driverFname = cusFirst;
            driverLname = cusLast;
            phonenumber = phoneN;
            email = cusEmail;
            driversLicense = driversLic;
            driversLExpire = driversExp;
            driversLicenseBday = bday;
            address_string = address;
            address_city = city;
            address_prov = prov;
            address_postalCode = postal;
            vehicle = cusVehicle;
            insuranceType = insurance;
            insurancePolicy = policy;
            vehicleOut = vOut;
            kmOut = kmO;
            driversProv = licProv;

        }

        public void openRental()
        {
            //Inserts into the customer table.
            #region Customer

            try
            {
                string dataSource = "dime.db";
                using (SQLiteConnection connection = new SQLiteConnection())
                {
                    connection.ConnectionString = "Data Source=" + dataSource;
                    connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand(connection))
                    {
                        command.CommandText =
                            "INSERT into customer  (fname, lname, address, city, prov, postal, phone, drivers_lic, drivers_exp, drivers_prov, email) VALUES (:fn, :ln, :ad, :ci, :pr, :po, :ph, :dL, :dE, :dP, :em)";
                        command.Parameters.Add("fn", DbType.String).Value = driverFname;
                        command.Parameters.Add("ln", DbType.String).Value = driverLname;
                        command.Parameters.Add("ad", DbType.String).Value = address_string;
                        command.Parameters.Add("ci", DbType.String).Value = address_city;
                        command.Parameters.Add("pr", DbType.String).Value = address_prov;
                        command.Parameters.Add("po", DbType.String).Value = address_postalCode;
                        command.Parameters.Add("ph", DbType.String).Value = phonenumber;
                        command.Parameters.Add("dL", DbType.String).Value = driversLicense;
                        command.Parameters.Add("dE", DbType.String).Value = driversLExpire;
                        command.Parameters.Add("dP", DbType.String).Value = driversProv;
                        command.Parameters.Add("em", DbType.String).Value = email;
                        command.ExecuteNonQuery();

                        //Sets up the var to reference customerID
                        customerID = connection.LastInsertRowId;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem loading in Customer Database. \n\n Please email rguarascia@gmail.com the following message\n\n" + ex.ToString(), "Customer Database Error");
            }
            #endregion

            vehicleType = getType(vehicle); //Gets type based on plate

            CPD = getPrice(vehicleType); //Gets price based on type

            insurnacePrice = getInsurancePirce(insuranceType).ToString(); //Gets insurance price based on type.

            //Inserts into the insurance table
            #region Insurance
            if (insuranceType != "None")
            {
                try
                {
                    string dataSource = "dime.db";
                    using (SQLiteConnection connection = new SQLiteConnection())
                    {
                        connection.ConnectionString = "Data Source=" + dataSource;
                        connection.Open();
                        using (SQLiteCommand command = new SQLiteCommand(connection))
                        {
                            command.CommandText =
                                "INSERT into insurance  (type, price, policy_no) VALUES (:ty, :pr, :pn)";
                            command.Parameters.Add("ty", DbType.String).Value = insuranceType;
                            command.Parameters.Add("pr", DbType.String).Value = insurnacePrice;
                            command.Parameters.Add("pn", DbType.String).Value = insurancePolicy;
                            command.ExecuteNonQuery();

                            //Sets up the var to reference customerID
                            insuranceID = connection.LastInsertRowId;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Problem loading in Customer Database. \n\n Please email rguarascia@gmail.com the following message\n\n" + ex.ToString(), "Customer Database Error");
                }
            }
            #endregion

            //Inserts into the rental table.
            #region rentals
            try
            {
                string dataSource = "dime.db";
                using (SQLiteConnection connection = new SQLiteConnection())
                {
                    connection.ConnectionString = "Data Source=" + dataSource;
                    connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand(connection))
                    {
                        command.CommandText =
                            "INSERT into rentals  (oDate, cDate, plate, customer_id, rate, employee_id, insurance_id, oKM, oFuel, iKM, iFuel) " +
                            "VALUES (:od, :cd, :vty, :cid, :ra, :eid, :iid, :okm, :ofuel, :ikm, :ifuel)";
                        command.Parameters.Add("od", DbType.String).Value = vehicleOut.ToShortDateString();
                        command.Parameters.Add("cd", DbType.String).Value = "0000/00/00"; //will update when closed
                        command.Parameters.Add("vty", DbType.String).Value = vehicle;
                        command.Parameters.Add("cid", DbType.String).Value = customerID; // customerID is set above
                        command.Parameters.Add("ra", DbType.String).Value = CPD; //Set
                        command.Parameters.Add("eid", DbType.String).Value = employeeID; //Set
                        command.Parameters.Add("iid", DbType.String).Value = insuranceID; // insuranceID is set above
                        command.Parameters.Add("okm", DbType.String).Value = kmOut;
                        command.Parameters.Add("ofuel", DbType.String).Value = 12; //Constant full tank set
                        command.Parameters.Add("ikm", DbType.String).Value = 0; //will update when closed
                        command.Parameters.Add("ifuel", DbType.String).Value = 0; //will update when closed;
                        command.ExecuteNonQuery();

                        //Sets up reference for the rentalID
                        rentalID = connection.LastInsertRowId;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem loading in Rentals Database. \n\n Please email rguarascia@gmail.com the following message\n\n" + ex.ToString(), "Rentals Database Error");
            }
            #endregion

        }

        //Returns a 2d array of current vehicles
        public string[,] getVehicles()
        {

            string[,] vehicles = new string[100, 6];

            SQLiteConnection sqlite_conn;          // Database Connection Object
            SQLiteCommand sqlite_cmd;             // Database Command Object
            SQLiteDataReader sqlite_datareader;  // Data Reader Object

            sqlite_conn = new SQLiteConnection("Data Source=dime.db;Version=3;New=True;");

            sqlite_conn.Open();

            sqlite_cmd = sqlite_conn.CreateCommand();

            sqlite_cmd.CommandText = "SELECT * FROM vehicles";

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            int count = 0;

            // The SQLiteDataReader allows us to run through each row per loop
            while (sqlite_datareader.Read()) // Read() returns true if there is still a result line to read
            {
                vehicles[count, 0] = sqlite_datareader.GetString(0);
                vehicles[count, 1] = sqlite_datareader.GetString(1);
                vehicles[count, 2] = sqlite_datareader.GetString(2);
                vehicles[count, 3] = sqlite_datareader.GetInt32(3).ToString();
                vehicles[count, 4] = sqlite_datareader.GetString(4);
                vehicles[count, 5] = sqlite_datareader.GetInt64(5).ToString();
                count++;
            }

            return vehicles;
        }

        //Removes all the crap form the phone number. 
        //Desired format is 1231231234
        public string toPhoneNumber(string usrNumber)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            return rgx.Replace(usrNumber, "");
        }

        // Gets type based on plate
        public string getType(string plate)
        {
            //SELECT class from vehicles
            //WHERE plate = "CBHN386";

            string vehType = "";

            SQLiteConnection sqlite_conn;
            SQLiteCommand sqlite_cmd;
            SQLiteDataReader sqlite_datareader;

            sqlite_conn = new SQLiteConnection("Data Source=dime.db;Version=3;New=True;");

            sqlite_conn.Open();

            sqlite_cmd = sqlite_conn.CreateCommand();

            sqlite_cmd.CommandText = "SELECT class FROM vehicles WHERE plate = :plate";

            sqlite_cmd.Parameters.Add("plate", DbType.String).Value = plate;

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                vehType = sqlite_datareader.GetString(0);
            }

            return vehType;
        }


        // Gets price based on Type
        public int getPrice(string type)
        {
            int price = 0;

            SQLiteConnection sqlite_conn;
            SQLiteCommand sqlite_cmd;
            SQLiteDataReader sqlite_datareader;

            sqlite_conn = new SQLiteConnection("Data Source=dime.db;Version=3;New=True;");

            sqlite_conn.Open();

            sqlite_cmd = sqlite_conn.CreateCommand();

            sqlite_cmd.CommandText = "SELECT price FROM vehicle_price WHERE type = :type";

            sqlite_cmd.Parameters.Add(":type", DbType.String).Value = type;

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                price = (sqlite_datareader.GetInt32(0));
            }

            return price;
        }

        // Gets insurance price based on type
        public int getInsurancePirce(string type)
        {
            int price = 0;

            SQLiteConnection sqlite_conn;
            SQLiteCommand sqlite_cmd;
            SQLiteDataReader sqlite_datareader;

            sqlite_conn = new SQLiteConnection("Data Source=dime.db;Version=3;New=True;");

            sqlite_conn.Open();

            sqlite_cmd = sqlite_conn.CreateCommand();

            sqlite_cmd.CommandText = "SELECT price FROM insurance_prices WHERE type = :type";

            sqlite_cmd.Parameters.Add(":type", DbType.String).Value = type;

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                price = (sqlite_datareader.GetInt32(0));
            }

            return price;
        }

        //Gets the amount of rentals created today and creates the id based on that
        //Rentals are created by getting today's date, counting how many created today, and adding that to the end
        public string getRentalId()
        {
            DateTime today = new DateTime();
            today = DateTime.Today;

            int count = 1;

            SQLiteConnection sqlite_conn;
            SQLiteCommand sqlite_cmd;
            SQLiteDataReader sqlite_datareader;

            sqlite_conn = new SQLiteConnection("Data Source=dime.db;Version=3;New=True;");

            sqlite_conn.Open();

            sqlite_cmd = sqlite_conn.CreateCommand();

            sqlite_cmd.CommandText = "SELECT oDate FROM rentals WHERE oDate = :date";


            sqlite_cmd.Parameters.Add(":date", DbType.String).Value = today.ToShortDateString();

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                if (sqlite_datareader.GetString(0) == today.ToShortDateString())
                {
                    count++;
                }
            }

            return today.ToShortDateString().Replace(@"-", string.Empty) + count; // yyyymmdd#
        }
    }
}
