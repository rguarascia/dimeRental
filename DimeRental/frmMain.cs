﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DimeRental
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AgreementForm newForm = new AgreementForm();
            newForm.status = "New";
            newForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AgreementForm newForm = new AgreementForm();
            newForm.status = "Close";
            newForm.Show();
        }

        private void btnVehMag_Click(object sender, EventArgs e)
        {
            frmVehicles frmVehicles = new frmVehicles();
            frmVehicles.Show();
        }

        private void btnIMS_Click(object sender, EventArgs e)
        {
            frmInsurancePrices frmInsurancePrices = new frmInsurancePrices();
            frmInsurancePrices.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmSearch frmSearch = new frmSearch();
            frmSearch.Show();
        }
    }
}
