﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DimeRental
{
    public partial class frmInsurancePrices : Form
    {
        SQLiteConnection sql_connection;
        SQLiteCommand sql_command;
        SQLiteDataReader sqlite_datareader;
        DataSet data_set;
        SQLiteDataAdapter sql_data_adapter;
        SQLiteCommandBuilder sql_command_builder;

        int row = 0;

        public frmInsurancePrices()
        {
            InitializeComponent();
        }

        public void SetConnection()
        {
            sql_connection = new SQLiteConnection(@"Data Source=dime.db");
            sql_connection.Open();
        }

        public void Loads()
        {
            SetConnection();
            data_set = new DataSet();

            sql_command = sql_connection.CreateCommand();
            string command_string = "select * from insurance_prices";

            sql_data_adapter = new SQLiteDataAdapter(command_string, sql_connection);
            sql_command_builder = new SQLiteCommandBuilder(sql_data_adapter);

            sql_data_adapter.Fill(data_set);

            dgvInsurnace.DataSource = data_set.Tables[0].DefaultView;

        }

        private void frmInsurancePrices_Load(object sender, EventArgs e)
        {
            Loads();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            row = dgvInsurnace.SelectedCells[0].RowIndex;
            txtPrice.Enabled = true;
            label1.Text = dgvInsurnace[0, row].Value.ToString() + " Price";
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Double newPrice;
            if (txtPrice.Text != "")
            {
                if (Double.TryParse(txtPrice.Text, out newPrice))
                {
                    string dataSource = "dime.db";
                    try
                    {
                        using (SQLiteConnection connection = new SQLiteConnection())
                        {
                            connection.ConnectionString = "Data Source=" + dataSource;
                            connection.Open();
                            using (SQLiteCommand command = new SQLiteCommand(connection))
                            {
                                command.CommandText =
                                    "update insurance_prices set price = :pr where type=:ty";
                                command.Parameters.Add("pr", DbType.String).Value = txtPrice.Text;
                                command.Parameters.Add("ty", DbType.String).Value = dgvInsurnace[0, row].Value.ToString();
                                command.ExecuteNonQuery();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Please include this message when emailing about a problem. This is the diagnostic information \n\nPress CTRL+C then paste into email body \n\n" + ex.ToString(), "We ran into a problem. Please contact rguarascia@gmail.com");
                    }
                    finally
                    {
                        Loads();
                    }
                }
                else
                {
                    MessageBox.Show("Please enter a number", "Parse Error");
                }
            }
            else
            {
                MessageBox.Show("Please enter a value for the new price", "Price Error");
            }
        }
    }
}
