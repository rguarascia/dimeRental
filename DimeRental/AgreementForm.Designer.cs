﻿namespace DimeRental
{
    partial class AgreementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRentLast = new System.Windows.Forms.TextBox();
            this.txtDriverLic = new System.Windows.Forms.Label();
            this.txtCustLast = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCustFirst = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDriversLicense = new System.Windows.Forms.TextBox();
            this.cmbDriverProv = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.cmbProv = new System.Windows.Forms.ComboBox();
            this.txtRentFirst = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPostal = new System.Windows.Forms.TextBox();
            this.cmbVehicle = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtKmOut = new System.Windows.Forms.TextBox();
            this.txtFuelOut = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtFuelIn = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtKmIn = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.radNone = new System.Windows.Forms.RadioButton();
            this.radOWC = new System.Windows.Forms.RadioButton();
            this.radNoDed = new System.Windows.Forms.RadioButton();
            this.label20 = new System.Windows.Forms.Label();
            this.txtPolicyNo = new System.Windows.Forms.TextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnFind = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.dtpOut = new System.Windows.Forms.DateTimePicker();
            this.dtpIn = new System.Windows.Forms.DateTimePicker();
            this.dtpExpiry = new System.Windows.Forms.DateTimePicker();
            this.label23 = new System.Windows.Forms.Label();
            this.dtpBirthday = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1023, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dime Rental Agreement Form";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(275, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Renter\'s Last Name";
            // 
            // txtRentLast
            // 
            this.txtRentLast.Location = new System.Drawing.Point(380, 64);
            this.txtRentLast.Name = "txtRentLast";
            this.txtRentLast.Size = new System.Drawing.Size(138, 20);
            this.txtRentLast.TabIndex = 3;
            // 
            // txtDriverLic
            // 
            this.txtDriverLic.AutoSize = true;
            this.txtDriverLic.Location = new System.Drawing.Point(275, 93);
            this.txtDriverLic.Name = "txtDriverLic";
            this.txtDriverLic.Size = new System.Drawing.Size(105, 13);
            this.txtDriverLic.TabIndex = 8;
            this.txtDriverLic.Text = "Customer Last Name";
            // 
            // txtCustLast
            // 
            this.txtCustLast.Location = new System.Drawing.Point(380, 90);
            this.txtCustLast.Name = "txtCustLast";
            this.txtCustLast.Size = new System.Drawing.Size(138, 20);
            this.txtCustLast.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Customer First Name";
            // 
            // txtCustFirst
            // 
            this.txtCustFirst.Location = new System.Drawing.Point(123, 90);
            this.txtCustFirst.Name = "txtCustFirst";
            this.txtCustFirst.Size = new System.Drawing.Size(138, 20);
            this.txtCustFirst.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(21, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(497, 19);
            this.label6.TabIndex = 9;
            this.label6.Text = "Renter\'s Information";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(250, 119);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Driver\'s Expiry";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 119);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Driver\'s License";
            // 
            // txtDriversLicense
            // 
            this.txtDriversLicense.Location = new System.Drawing.Point(106, 116);
            this.txtDriversLicense.Name = "txtDriversLicense";
            this.txtDriversLicense.Size = new System.Drawing.Size(138, 20);
            this.txtDriversLicense.TabIndex = 11;
            // 
            // cmbDriverProv
            // 
            this.cmbDriverProv.FormattingEnabled = true;
            this.cmbDriverProv.Items.AddRange(new object[] {
            "AB",
            "BC",
            "MB",
            "NB",
            "NL",
            "NT",
            "NS",
            "NU",
            "ON",
            "PE",
            "QC",
            "SK",
            "YT"});
            this.cmbDriverProv.Location = new System.Drawing.Point(473, 116);
            this.cmbDriverProv.Name = "cmbDriverProv";
            this.cmbDriverProv.Size = new System.Drawing.Size(45, 21);
            this.cmbDriverProv.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(524, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(497, 19);
            this.label4.TabIndex = 16;
            this.label4.Text = "Vehicle Information";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 145);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Driver\'s Birthday";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 171);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Address";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(69, 168);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(192, 20);
            this.txtAddress.TabIndex = 19;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(267, 171);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(24, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "City";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(297, 168);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(138, 20);
            this.txtCity.TabIndex = 21;
            // 
            // cmbProv
            // 
            this.cmbProv.FormattingEnabled = true;
            this.cmbProv.Items.AddRange(new object[] {
            "Alberta",
            "British Columbia",
            "Manitoba",
            "New Brunswick",
            "Newfoundland and Labrador",
            "Northwest Territories",
            "Nova Scotia",
            "Nunavut",
            "Ontario",
            "Prince Edward Island",
            "Quebec",
            "Saskatchewan",
            "Yukon"});
            this.cmbProv.Location = new System.Drawing.Point(441, 168);
            this.cmbProv.Name = "cmbProv";
            this.cmbProv.Size = new System.Drawing.Size(77, 21);
            this.cmbProv.TabIndex = 23;
            // 
            // txtRentFirst
            // 
            this.txtRentFirst.Location = new System.Drawing.Point(123, 64);
            this.txtRentFirst.Name = "txtRentFirst";
            this.txtRentFirst.Size = new System.Drawing.Size(138, 20);
            this.txtRentFirst.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Renter\'s First Name";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(18, 197);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "Postal Code";
            // 
            // txtPostal
            // 
            this.txtPostal.Location = new System.Drawing.Point(123, 194);
            this.txtPostal.Name = "txtPostal";
            this.txtPostal.Size = new System.Drawing.Size(138, 20);
            this.txtPostal.TabIndex = 24;
            // 
            // cmbVehicle
            // 
            this.cmbVehicle.FormattingEnabled = true;
            this.cmbVehicle.Location = new System.Drawing.Point(640, 64);
            this.cmbVehicle.Name = "cmbVehicle";
            this.cmbVehicle.Size = new System.Drawing.Size(381, 21);
            this.cmbVehicle.TabIndex = 26;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(588, 67);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "Vehicle ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(588, 98);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "KM Out";
            // 
            // txtKmOut
            // 
            this.txtKmOut.Location = new System.Drawing.Point(640, 93);
            this.txtKmOut.Name = "txtKmOut";
            this.txtKmOut.Size = new System.Drawing.Size(100, 20);
            this.txtKmOut.TabIndex = 29;
            // 
            // txtFuelOut
            // 
            this.txtFuelOut.Location = new System.Drawing.Point(920, 91);
            this.txtFuelOut.Name = "txtFuelOut";
            this.txtFuelOut.Size = new System.Drawing.Size(100, 20);
            this.txtFuelOut.TabIndex = 31;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(871, 95);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "Fuel Out";
            // 
            // txtFuelIn
            // 
            this.txtFuelIn.Location = new System.Drawing.Point(920, 116);
            this.txtFuelIn.Name = "txtFuelIn";
            this.txtFuelIn.Size = new System.Drawing.Size(100, 20);
            this.txtFuelIn.TabIndex = 35;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(871, 120);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 13);
            this.label16.TabIndex = 34;
            this.label16.Text = "Fuel In";
            // 
            // txtKmIn
            // 
            this.txtKmIn.Location = new System.Drawing.Point(640, 118);
            this.txtKmIn.Name = "txtKmIn";
            this.txtKmIn.Size = new System.Drawing.Size(100, 20);
            this.txtKmIn.TabIndex = 33;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(588, 123);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 32;
            this.label17.Text = "KM In";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(524, 171);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(497, 19);
            this.label18.TabIndex = 36;
            this.label18.Text = "Insurance Information";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(628, 199);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 37;
            this.label19.Text = "Insurance";
            // 
            // radNone
            // 
            this.radNone.AutoSize = true;
            this.radNone.Location = new System.Drawing.Point(688, 197);
            this.radNone.Name = "radNone";
            this.radNone.Size = new System.Drawing.Size(51, 17);
            this.radNone.TabIndex = 38;
            this.radNone.TabStop = true;
            this.radNone.Text = "None";
            this.radNone.UseVisualStyleBackColor = true;
            // 
            // radOWC
            // 
            this.radOWC.AutoSize = true;
            this.radOWC.Location = new System.Drawing.Point(745, 197);
            this.radOWC.Name = "radOWC";
            this.radOWC.Size = new System.Drawing.Size(57, 17);
            this.radOWC.TabIndex = 39;
            this.radOWC.TabStop = true;
            this.radOWC.Text = "O.W.C";
            this.radOWC.UseVisualStyleBackColor = true;
            // 
            // radNoDed
            // 
            this.radNoDed.AutoSize = true;
            this.radNoDed.Location = new System.Drawing.Point(808, 197);
            this.radNoDed.Name = "radNoDed";
            this.radNoDed.Size = new System.Drawing.Size(93, 17);
            this.radNoDed.TabIndex = 40;
            this.radNoDed.TabStop = true;
            this.radNoDed.Text = "No Deductible";
            this.radNoDed.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(628, 223);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(105, 13);
            this.label20.TabIndex = 42;
            this.label20.Text = "Insurance Policy No.";
            // 
            // txtPolicyNo
            // 
            this.txtPolicyNo.Location = new System.Drawing.Point(739, 220);
            this.txtPolicyNo.Name = "txtPolicyNo";
            this.txtPolicyNo.Size = new System.Drawing.Size(162, 20);
            this.txtPolicyNo.TabIndex = 41;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(945, 250);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 43;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(864, 250);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 44;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(524, 64);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(35, 23);
            this.btnFind.TabIndex = 45;
            this.btnFind.Text = "Find";
            this.btnFind.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(275, 148);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(78, 13);
            this.label21.TabIndex = 47;
            this.label21.Text = "Phone Number";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(359, 145);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(159, 20);
            this.txtPhone.TabIndex = 46;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(275, 197);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 13);
            this.label22.TabIndex = 49;
            this.label22.Text = "Email";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(359, 194);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(159, 20);
            this.txtEmail.TabIndex = 48;
            // 
            // dtpOut
            // 
            this.dtpOut.Location = new System.Drawing.Point(17, 253);
            this.dtpOut.Name = "dtpOut";
            this.dtpOut.Size = new System.Drawing.Size(135, 20);
            this.dtpOut.TabIndex = 50;
            // 
            // dtpIn
            // 
            this.dtpIn.Location = new System.Drawing.Point(383, 253);
            this.dtpIn.Name = "dtpIn";
            this.dtpIn.Size = new System.Drawing.Size(135, 20);
            this.dtpIn.TabIndex = 51;
            // 
            // dtpExpiry
            // 
            this.dtpExpiry.Location = new System.Drawing.Point(329, 116);
            this.dtpExpiry.Name = "dtpExpiry";
            this.dtpExpiry.Size = new System.Drawing.Size(67, 20);
            this.dtpExpiry.TabIndex = 52;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(418, 120);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 13);
            this.label23.TabIndex = 53;
            this.label23.Text = "Province";
            // 
            // dtpBirthday
            // 
            this.dtpBirthday.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpBirthday.Location = new System.Drawing.Point(107, 142);
            this.dtpBirthday.Name = "dtpBirthday";
            this.dtpBirthday.Size = new System.Drawing.Size(155, 20);
            this.dtpBirthday.TabIndex = 54;
            // 
            // AgreementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 285);
            this.Controls.Add(this.dtpBirthday);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.dtpExpiry);
            this.Controls.Add(this.dtpIn);
            this.Controls.Add(this.dtpOut);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtPolicyNo);
            this.Controls.Add(this.radNoDed);
            this.Controls.Add(this.radOWC);
            this.Controls.Add(this.radNone);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtFuelIn);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtKmIn);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtFuelOut);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtKmOut);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cmbVehicle);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtPostal);
            this.Controls.Add(this.cmbProv);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtCity);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbDriverProv);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtDriversLicense);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtDriverLic);
            this.Controls.Add(this.txtCustLast);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCustFirst);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtRentLast);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtRentFirst);
            this.Controls.Add(this.label1);
            this.Name = "AgreementForm";
            this.Text = "Agreement Form";
            this.Load += new System.EventHandler(this.AgreementForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRentLast;
        private System.Windows.Forms.Label txtDriverLic;
        private System.Windows.Forms.TextBox txtCustLast;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCustFirst;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDriversLicense;
        private System.Windows.Forms.ComboBox cmbDriverProv;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.ComboBox cmbProv;
        private System.Windows.Forms.TextBox txtRentFirst;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPostal;
        private System.Windows.Forms.ComboBox cmbVehicle;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtKmOut;
        private System.Windows.Forms.TextBox txtFuelOut;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtFuelIn;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtKmIn;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.RadioButton radNone;
        private System.Windows.Forms.RadioButton radOWC;
        private System.Windows.Forms.RadioButton radNoDed;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtPolicyNo;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.DateTimePicker dtpOut;
        private System.Windows.Forms.DateTimePicker dtpIn;
        private System.Windows.Forms.DateTimePicker dtpExpiry;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DateTimePicker dtpBirthday;
    }
}